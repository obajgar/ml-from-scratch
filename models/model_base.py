from configuration import ConfigurableObject


class ModelBase(ConfigurableObject):

    """
    Base class for ML models and layers. Should implement a __call__ method that implements the application of the model
    to an input. params() should return a list of all trainable parameters.
    """

    def __init__(self, long_name='model',  **kwargs):
        super().__init__(long_name, **kwargs)
        self._params = []

    def __call__(self, *args, **kwargs):
        raise NotImplementedError

    @property
    def params(self):
        """
        :return: list(Tensor) - list of all trainable parameters of the model
        """
        childrens_params = []
        for child in self._children:
            childrens_params += child.params
        return self._params + childrens_params

    @property
    def total_params(self):
        """
        :return: int - number of trainable parameters of the model
        """
        return sum(p.numel() for p in self.params)
