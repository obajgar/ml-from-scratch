from models.model_base import ModelBase
from models import MLPModel

import torch


class ConvLayer:
    """
    Single convolutional layer
    """
    def __init__(self, n_filters, filter_dims, offset=1, in_dims=None, precompute_masks=True):
        self._n_filters = n_filters
        self._filter_dims = filter_dims
        self._offset = offset

        self.filters = None

        self.initialize_filters()
        self._in_dims = in_dims if precompute_masks else None

        if precompute_masks:
            assert in_dims is not None, "Must provide in_dims to precompute masks."

            self.initialize_precompute()


    def initialize_filters(self):

        self.filters = torch.randn((self._n_filters, *self._filter_dims), requires_grad=True)

    def initialize_precompute(self):
        """
        IGNORE: so far doesn't work well in practice.
        Creates a multidimensional tensor filter containing the base filter at each position at which it will eventually
        be applied to use at runtime instead of the two slow for loops.
        :return:
        """

        print("Warning! Precomputing the convolution masks is not recommended. Current implementation doesn't seem "
              "to work well in practice.")

        # Creates an [n_filters]x[out_height]x[out_width]x[in_channels]x[in_height]x[in_width] tensor

        out_h = int((self._in_dims[0] - self._filter_dims[1]) / self._offset + 1)
        out_w = int((self._in_dims[1] - self._filter_dims[2]) / self._offset + 1)

        self._filter_fhwchw = torch.zeros((self._n_filters, out_h, out_w, self._filter_dims[0], *self._in_dims))

        for i in torch.arange(out_h):
            for j in torch.arange(out_w):
                io = i * self._offset
                jo = j * self._offset
                self._filter_fhwchw[:, i, j, :, io:(io + self._filter_dims[1]), jo:(jo + self._filter_dims[2])] = self.filters

    def __call__(self, x_bchw: torch.Tensor):
        """

        :param x_bchw: torch.Tensor of shape [batch_size]x[n_channels]x[height]x[width]
        :return:
        """

        input_dims = x_bchw.size()

        assert input_dims[1] == self._filter_dims[0], "The number of input channels must correspond to the number of filter channels."

        out_h = int((input_dims[2] - self._filter_dims[1]) / self._offset + 1)
        out_w = int((input_dims[3] - self._filter_dims[2]) / self._offset + 1)

        if self._in_dims:
            x_b111chw = x_bchw[:,None,None,None,:,:,:]
            out_bfhw = torch.sum(torch.sum(torch.sum(
                                    torch.multiply(self._filter_fhwchw, x_b111chw),
                                    dim=6),dim=5), dim=4)

        else:
            out_bfhw = torch.zeros((input_dims[0], self._n_filters, out_h, out_w))

            x_b1chw = x_bchw.unsqueeze(1)

            for i in torch.arange(out_h):
                for j in torch.arange(out_w):
                    io = i * self._offset
                    jo = j * self._offset
                    x_window_b1chw = x_b1chw[:, :, :, io:(io + self._filter_dims[1]), jo:(jo + self._filter_dims[2])]
                    product_bfchw = torch.multiply(self.filters, x_window_b1chw)
                    scores_bf = torch.sum(torch.sum(torch.sum(product_bfchw, dim=4), dim=3), dim=2)
                    out_bfhw[:, :, i, j] = scores_bf

        return out_bfhw

    @property
    def params(self):
        return [self.filters]


class MaxPool:
    """
    Single maxpooling layer
    """
    def __init__(self, filter_dims, offset=1, in_dims=None, precompute_masks=True):
        self._filter_dims = filter_dims
        self._offset = offset
        self._in_dims = in_dims if precompute_masks else None

        if precompute_masks:
            assert in_dims is not None, "Must provide in_dims to precompute masks."
            print("WARNING: Precomputed masks don't seem to work well and are not recommended!")
            self._precompute_masks()

    def _precompute_masks(self):
        out_h = int((self._in_dims[0] - self._filter_dims[1]) / self._offset + 1)
        out_w = int((self._in_dims[1] - self._filter_dims[2]) / self._offset + 1)

        self._mask_chwhw = torch.zeros((self._filter_dims[0], out_h, out_w, *self._in_dims))
        ones_chw = torch.ones(self._filter_dims)

        for i in torch.arange(out_h):
            for j in torch.arange(out_w):
                io = i * self._offset
                jo = j * self._offset
                self._mask_chwhw[:, i, j, io:(io + self._filter_dims[1]), jo:(jo + self._filter_dims[2])] = ones_chw

    def __call__(self, x_bchw: torch.Tensor):
        """

        :param x_bchw: torch.Tensor of shape [batch_size]x[n_channels]x[height]x[width]
        :return:
        """

        input_dims = x_bchw.size()

        out_h = int((input_dims[2] - self._filter_dims[1]) / self._offset + 1)
        out_w = int((input_dims[3] - self._filter_dims[2]) / self._offset + 1)

        if self._in_dims:
            out_bchw = torch.max(torch.max(
                            torch.multiply(self._mask_chwhw, x_bchw[:,:,None,None,:,:]),
                            dim=5)[0], dim=4)[0]

        else:

            out_bchw = torch.zeros((input_dims[0], input_dims[1], out_h, out_w))

            for i in torch.arange(out_h):
                for j in torch.arange(out_w):
                    io = i * self._offset
                    jo = j * self._offset
                    out_bchw[:, :, i, j] = torch.max(torch.max(
                        x_bchw[:, :, io:io + self._filter_dims[1], jo:jo + self._filter_dims[2]],
                        dim=3)[0], dim=2)[0]

        return out_bchw

    @property
    def params(self):
        return []


class ConvolutionalModel(ModelBase):
    """
    Simple convolutional neural network consisting of multiple layers of convolutional and maxpooling layers
    followed by a fully connected MLP.
    """

    def __init__(self, input_channels=1, long_name='conv', short_name='conv', input_dims=(28, 28),
                 add_channel_dim=True):
        """

        :param input_channels: int - number of channels input into the first layer (typically 1 for grayscale images
                                     and 3 for RGB images
        :param long_name: str - used mainly for prefixing other config parameters
        :param short_name:
        :param input_dims: tuple(int) - dimensions of the input images
        :param add_channel_dim: bool - whether to automatically add a dimension for a singleton channel
        """
        super().__init__(long_name=long_name, short_name=short_name)

        # Use an MLP model as the fully connected layers:
        self.add_child(MLPModel(long_name='fully_connected'))

        self.add_parameter('n_filters', type=list, default=(32,),
                           description="A list of numbers of filters in each conv layer.")
        self.add_parameter('filter_size', type=list, default=(8,),
                           description="List of filter sizes for each layer. int creates a square filter. tuple can encode size in each dimension.")
        self.add_parameter('offset', type=list, default=(2,),
                           description="List of offsets for each layer. int creates a square filter. tuple can encode offset in each dimension.")
        self.add_parameter('maxpool_size', type=list, default=(4,),
                           description="List of maxpooling window sizes for each layer. int creates a square filter. tuple can encode size in each dimension.")
        self.add_parameter('maxpool_offset', type=list, default=(2,),
                           description="List of maxpool window offsets for each layer. int creates a square filter. tuple can encode size in each dimension.")
        self.add_parameter('precompute_masks', type=bool, default=False,
                           description="Precomputes tensors containing filters/maxpool masks at each position to avoid"
                                       "for loops at runtime.")

        self._input_channels = input_channels
        self._input_dims = input_dims
        self._add_channel_dim = add_channel_dim

    @staticmethod
    def create_filter_dims(n_channels, filter_size):
        """

        :param n_channels: number of input channels of the given conv layer
        :param filter_size: int (for a square filter) or pair of ints (dims of a 2-D filter)
        :return: tuple (n_channels, filter_height, filter_width)
        """

        if isinstance(filter_size, int):
            filter_size = (n_channels, filter_size, filter_size)
        else:
            filter_size = (n_channels, *filter_size)

        return filter_size

    def initialize(self):

        self._conv_layers = []
        n_channels = self._input_channels

        dims = [n_channels, *self._input_dims]

        # Alternates in creating convolutional and maxpooling layers
        for n_filters, filter_size, offset, mp_size, mp_offset \
                in zip(self['n_filters'], self['filter_size'], self['offset'], self['maxpool_size'],
                       self['maxpool_offset']):

            filter_size = self.create_filter_dims(n_channels, filter_size)
            mp_size = self.create_filter_dims(n_filters, mp_size)

            # Create conv layer
            self._conv_layers.append(ConvLayer(n_filters, filter_size, offset, in_dims=(dims[1], dims[2]),
                                               precompute_masks=self["precompute_masks"]))
            # Calculate the dimensions of the layer's output
            dims = (n_filters, int((dims[1] - filter_size[1]) / offset) + 1, int((dims[2] - filter_size[2]) / offset) + 1)
            n_channels = n_filters
            print(f"Conv layer output dim: \t {dims}")
            print(f"Conv layer params: \t {self._conv_layers[-1].filters.numel()}")

            # Create a maxpooling layer:
            self._conv_layers.append(MaxPool(mp_size, mp_offset, in_dims=(dims[1], dims[2]),
                                             precompute_masks=self["precompute_masks"]))
            dims = (n_filters, int((dims[1] - mp_size[1]) / mp_offset) + 1, int((dims[2] - mp_size[2]) / mp_offset) + 1)
            print(f"Maxpooling output dims: \t {dims}")

        self.fully_connected.initialize(in_dim=int(dims[0] * dims[1] * dims[2]))

        print(f"CNN fully connected module params: {self.fully_connected.total_params}")

    def __call__(self, x_bchw):

        if self._add_channel_dim:
            hidden_bchw = x_bchw.unsqueeze(1)
        else:
            hidden_bchw = x_bchw

        # Apply each convolutinal and maxpooling layer
        for l in self._conv_layers:
            hidden_bchw = l(hidden_bchw)

        # Flatten the output of the last layer to pass it to the fully connected layers
        hid_dims = hidden_bchw.size()
        hidden_bf = torch.reshape(hidden_bchw, (hid_dims[0], -1))

        # Apply the MLP model
        y_b = self.fully_connected(hidden_bf)

        return y_b

    @property
    def params(self):
        conv_params = []
        for l in self._conv_layers:
            conv_params += l.params
        return conv_params + self.fully_connected.params
