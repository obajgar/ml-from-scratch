from models.model_base import ModelBase
import torch

activation_dict = {
    'linear': lambda x: x,
    'sigmoid': torch.sigmoid,
    'tanh': torch.tanh,
    'relu': torch.relu
}

class MLPModel(ModelBase):
    """
    A multilayer perceptron model in Torch.
    """

    compatible_backends = ['Torch']

    def __init__(self, default_dims=None, default_activation='linear', last_layer_activation='linear', **kwargs):
        super().__init__(**kwargs)
        self.add_parameter('dims', 'd', type=list, default=default_dims)
        self.add_parameter('activation', 'a', type=str, default=default_activation)
        self.add_parameter('last_activation', 'la', type=str, default=last_layer_activation)
        self.add_parameter('bias', 'b', type=bool, default=True)

        self._weights = []
        self._biases = []
        self._activations = []

    def initialize(self, in_dim=None):
        hidden_activation = activation_dict[self['activation']]
        self._activations = []

        if in_dim:
            self['dims'][0] = in_dim
        for layer in range(len(self['dims'])-1):
            self._weights.append(torch.randn((self['dims'][layer+1], self['dims'][layer]), requires_grad=True))
            self._activations.append(hidden_activation)
            bias = 0 if not self['bias'] else torch.randn((self['dims'][layer+1],1), requires_grad=True)
            self._biases.append(bias)
        self._activations[-1] = activation_dict[self['last_activation']]

    def __call__(self, x_b, **kwargs):

        res = x_b
        if len(res.shape) == 2:
            # Turn into a column vector
            res = torch.reshape(res, (*res.shape,1))

        for W, b, activation in zip(self._weights, self._biases, self._activations):
            res = activation(torch.matmul(W, res) + b)

        return res

    @property
    def params(self):
        return self._weights + self._biases