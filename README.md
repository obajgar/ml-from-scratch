# Machine learning from scratch
A practice project implementing neural network architectures and their training from scratch. (so far depends on Torch's autograd for automated differentiation) 

Implementations of basic components:
- `models/`
  - `mlp.py` contains and implementation of a multilayer perceptron
  - `convolutional.py` contains and implementation of a simple convolutional neural network (CNN)

- `optimization/`
  - `sgd.py` implements the gradient descent update step
  - `adam.py` implements the Adam optimizaer

- `trainers/`
  - `trainer_base.py` implements the main training loop
  - `pytorch_trainer.py` calculates the gradient using torch's autograd implementation
- `evaluators/` contains implementations of multiple error functions


### Prerequisites:
- With a new virtual env:
  - Install core dependencies:
    - `pip install torch`
  - **Optional dependencies** (without `ord` installed, run with `--trainer.diable_ord True`):
    - `pip install git+https://gitlab.com/obajgar/ord.git`  (my logging module)
    - `pip install comet_ml` (to use logging into CometML, please include a `comet_config.py` file into the `config` dir containing a dictionary `comet_conf` with the API access token and configuration)
    - `pip install plotly` (for plotting training curves)

### Run experiment:
Currently, it should be possible to run the following experiment out of the box to train on MNIST and get >90% accuracy:
`python experiments/mnist_conv.py`
or 
`python experiments/mnist_mlp.py`


Example training curves on MNIST:
![](result_analysis/mlp_both.png)
