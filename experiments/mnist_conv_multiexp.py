from trainers import PytorchTrainer
from models import ConvolutionalModel
from datasets import MnistDataset
from evaluators import CrossEntropy, Accuracy
from optimization import SGD
import torch
from configuration import configuration
from utils import should_stop

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'


common_conf = {
    'conv.fully_connected.activation': 'sigmoid',
    'conv.fully_connected.dims': [None, 32, 32, 10],
    'trainer.epochs': 400,
    'trainer.opt.lr_decay': True,
    'dataset.batch_size': 256,
    'trainer.enable_comet': True

}

n_experiments = 4

param_sampling_sets = {
    'trainer.opt.learning_rate': [0.05,0.1,0.2,0.4,0.8],
    'loss.regularization': [0,0.001,0.0001],
    'conv.filter_size': ((4,),(5,),(6,),(7,)),
    'conv.offset': ((1,),(2,))
}

for n in range(n_experiments):

    if should_stop():
        break

    configuration.reset()
    configuration.add_config(common_conf)

    train_data = MnistDataset(flat=False)
    valid_data = MnistDataset(subset='val', flat=False, long_name='val_data')

    model = ConvolutionalModel()

    trainer = PytorchTrainer(model, CrossEntropy(), train_data, SGD(),
                             extra_evaluators=[Accuracy()],
                             valid_dataset=valid_data)

    configuration.randomly_sample(param_sampling_sets)
    trainer.run_training()

    trainer.plot_training_curves()

