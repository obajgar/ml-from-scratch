from trainers import PytorchTrainer
from models import MLPModel
from datasets import SyntheticDataset
from evaluators import MSE
from optimization import SGD
import torch

import os

os.environ['KMP_DUPLICATE_LIB_OK']='True'


import numpy as np

dims = [5, 3]

ground_truth_coefficient = torch.randn([1,dims[1], dims[0]])

model = MLPModel(dims)
train_data = SyntheticDataset(ground_truth_coefficient, dims=dims)
test_data = SyntheticDataset(ground_truth_coefficient, dims=dims, long_name='test_data')
valid_data = SyntheticDataset(ground_truth_coefficient, dims=dims, long_name='valid_data')

trainer = PytorchTrainer(model, MSE(), train_data, SGD(), valid_dataset=valid_data, test_dataset=test_data)
trainer.run_training()

trainer.plot_training_curves()