from optimization.optimizer_base import OptimizerBase
import torch
from models.model_base import ModelBase

class Adam(OptimizerBase):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.add_parameter("beta1", "b1", type=float, default=0.9)
        self.add_parameter("beta2", "b2", type=float, default=0.999)
        self.add_parameter("epsilon", "e", type=float, default=1e-8)

        self._mt = []
        self._vt = []

        self._first_update = True

        self._it = torch.tensor(0)

    def initialize(self):

        print("Initializing adam")

        self._beta1 = torch.tensor(self['beta1'])
        self._beta2 = torch.tensor(self['beta2'])
        self._eps = torch.tensor(self['epsilon'])
        self._lr = torch.tensor(self['learning_rate'])



    def step(self, model: ModelBase, gradients: list):

        with torch.no_grad():

            self._it += 1

            if self._first_update:
                self._vt = torch.zeros((len(gradients),))
                self._mt = [torch.zeros_like(w) for w in model.params]


            # Gradient update for each model parameter
            for w, grad, mt, vt in zip(model.params, gradients, self._mt, self._vt):
                mt *= self._beta1
                mt += (1-self._beta1) * grad

                vt *= self._beta2
                vt += (1-self._beta2) * torch.square(torch.norm(grad))

                mhat = mt/(1-self._beta1**self._it)
                vhat = vt/(1-self._beta2**self._it)

                w -= (self._lr / (torch.sqrt(vhat)+self._eps)) * mt
                w.grad.zero_()
