from configuration import ConfigurableObject


class OptimizerBase(ConfigurableObject):
    """
    Base class for optimizers performing parameter updates using gradients.
    """

    def __init__(self, long_name='opt', short_name='o', **kwargs):
        super().__init__(long_name=long_name, short_name=short_name, **kwargs)
        self.add_parameter('learning_rate', 'lr', type=float, default=0.01)

    def step(self, weights: list, gradients: list):
        """
        Should update each weight in the weights list using the current gradient.

        :param weights: list(Tensor) - a list of parameter tensors to update
        :param gradients: list(Tensor) - a list of gradients corresponding to each of the parameters
        :return: None
        """
        raise NotImplementedError
