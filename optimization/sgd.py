from optimization.optimizer_base import OptimizerBase
from models.model_base import ModelBase
import torch
import numpy as np

class SGD(OptimizerBase):
    """
    Implementation of vanilla (stochastic) gradient descent with optional simulated annealing of the learning rate.
    """

    def __init__(self, lr_decay=False, **kwargs):
        super().__init__(**kwargs)
        self.add_parameter('lr_decay', type=bool, default=lr_decay,
                           description="If true, decays the learning rate as 1/sqrt(epoch)")
        # Current iteration
        self._it = torch.tensor(0.)

    def step(self, model: ModelBase, gradients: list):

        self._it += 1

        with torch.no_grad():

            # Current learning rate calculation
            if self['lr_decay']:
                lr = self['learning_rate'] / np.sqrt(self._parent.current_epoch)
            else:
                lr = self['learning_rate']

            # Gradient update for each model parameter
            for w, grad in zip(model.params, gradients):
                w -= lr * grad
                w.grad.zero_()
