import torch
from evaluators.evaluator_base import EvaluatorBase

class CrossEntropy(EvaluatorBase):

    def __init__(self, long_name='loss'):
        super().__init__(long_name=long_name)

        self.add_parameter('regularization', type=float, default=0,
                           description='The strength of L2 regularization.')

    def __call__(self, y_bc, y_pred_bc, weights=None, logits=True, *args, **kwargs):
        """

        :param y_bc: Tensor [batch_size] x [n_classes]
        :param y_pred_bc: Tensor [batch_size] x [n_classes]
        :param weights: list(Tensor) - weights used for regularization
        :param logits: if True, expects probabilities to be logits over the whole real line. Else expects probabilities in (0,1).
        :param args:
        :param kwargs:
        :return:
        """

        assert weights is not None or self['regularization'] == 0, \
            "Non-zero regularization term requires weights to calculate the loss."

        y_pred_bc = torch.squeeze(y_pred_bc)

        if logits:
            loss = torch.mean(- torch.sum(y_pred_bc*y_bc, dim=1) + torch.logsumexp(y_pred_bc, dim=1))
        else:
            ### WARNING: This is not a numerically stabilised computation:
            loss = - torch.mean(y_bc * torch.log(y_pred_bc) + (1 - y_bc) * torch.log(1 - y_pred_bc))

        if self['regularization']:
            reg = 0.5 * self['regularization'] * sum([torch.square(torch.norm(w)) for w in weights])
            loss += reg

        return loss