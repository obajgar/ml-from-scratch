import plotly.graph_objects as go
from result_analysis.utils import extract_df


def plot_training_curves(results: list, metrics: list, index="tot_iter"):
    """
    Plots metrics logged in results as a function of index using plotly.
    :param results: list(dict)
    :param metrics: list(str)
    :param index: str
    :return:
    """
    res_df = extract_df(results, cols=metrics, index=index)

    fig = go.Figure()
    for m in metrics:
        fig.add_trace(go.Scatter(x=res_df.index, y=res_df[m], name=m))
    fig.update_layout(xaxis_title=index, yaxis_title="metric", yaxis_type='log')
    fig.show()
