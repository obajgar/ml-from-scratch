from datasets.dataset_base import DatasetBase
import struct
import numpy as np
import os
import gzip
import requests
import shutil

def gunzip_shutil(source_filepath, dest_filepath, block_size=65536):
    with gzip.open(source_filepath, 'rb') as s_file, \
            open(dest_filepath, 'wb') as d_file:
        shutil.copyfileobj(s_file, d_file, block_size)


default_data_dir = '/Users/ob/data/datasets/vision/MNIST'
data_files = {
    'train': {
        'im': 'train-images-idx3-ubyte',
        'lab': 'train-labels-idx1-ubyte'
    },
    'val': {
        'im': 't10k-images-idx3-ubyte',
        'lab': 't10k-labels-idx1-ubyte'
    }
}
download_root = 'http://yann.lecun.com/exdb/mnist/'

class MnistDataset(DatasetBase):

    def __init__(self, subset='train', data_dir=default_data_dir, flat=False, onehot=True, **kwargs):
        """

        :param subset: 'train' or 'val'
        """
        super().__init__(**kwargs)

        self.add_parameter('data_dir', type=str, default=data_dir)

        self._image_file = data_files[subset]['im']
        self._label_file = data_files[subset]['lab']

        self._data_dir = data_dir
        self._flat = flat
        self._onehot = onehot

        self._images = None
        self._labels = None
        self._dims = None
        self._out_dims = None

    def initialize(self):
        self.load_data()

    def get_epoch_iterator(self):

        new_order = np.random.permutation(self._n_examples)

        # Discard last incomplete batch:
        # TODO: Fix this!
        new_order = new_order[:self['batch_size']*int(self._n_examples/self['batch_size'])]

        # Put images and labels into batches:
        im_bnxy = np.reshape(self._images[new_order],
                             (-1, self['batch_size'], *self._dims))

        lab_bn = np.reshape(self._labels[new_order],
                            (-1, self['batch_size'], *self._out_dims))

        return zip(im_bnxy, lab_bn)

    def load_data(self):

        _image_file_path = os.path.join(self['data_dir'], self._image_file)
        _label_file_path = os.path.join(self['data_dir'], self._label_file)

        if not os.path.exists(_image_file_path):
            self.dowload_data(unzip=True)

        # Load images
        with open(_image_file_path, 'rb') as f:
            print(f"Loading images from {_image_file_path}")
            magic, size = struct.unpack(">II", f.read(8))
            self._dims = struct.unpack(">II", f.read(8))
            if self._flat:
                print(self._dims)
                self._dims = (self._dims[0]*self._dims[1],)
            data = np.fromfile(f, dtype=np.dtype(np.uint8).newbyteorder('>'))
            shaped_data = data.reshape((size, *self._dims))
            self._images = shaped_data.astype('float32')/256.

        # Load lables
        with open(_label_file_path, 'rb') as f:
            print(f"Loading labels from {_label_file_path}")

            magic, n_labels = struct.unpack(">II", f.read(8))
            self._labels = np.fromfile(f, dtype=np.dtype(np.uint8).newbyteorder('>'))

        assert size == n_labels
        self._n_examples = size

        if self._onehot:
            one_hot_labels = np.zeros((size, 10))
            one_hot_labels[np.arange(size), self._labels] = 1
            self._labels = one_hot_labels

            self._out_dims = (10,)

        else:
            self._out_dims = (1,)

    def dowload_data(self, unzip=False):
        os.makedirs(self._data_dir, exist_ok=True)
        for ss in ['train', 'val']:
            print(f"Downloading {ss} data ...")
            for d in ['im', 'lab']:
                file_name = data_files[ss][d]
                file = requests.get(download_root+file_name+".gz")
                target_file= os.path.join(self._data_dir, file_name+".gz")
                open(target_file, 'wb').write(file.content)
                print("Downloaded to " + target_file)
                if unzip:
                    gunzip_shutil(target_file, os.path.join(self._data_dir, file_name))
        print("Finished downloading.")

        if unzip:
            import gzip
            import shutil



    @property
    def dims(self):
        return self._dims